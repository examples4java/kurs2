/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multitask;

import FutureClass.FutureExample;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import timeutils.MeasureTime;

/**
 *
 * @author tez
 */
public class Multitask {
    
    private static final int THREADS_COUNT = 2;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        MeasureTime t = new MeasureTime(true);
        
        System.out.println("Operacja na dwóch wątkach:");
//        MyThread t1 = new MyThread(0,1000000000/2);
//        MyThread t2 = new MyThread(1000000000/2,1000000000);
//        t1.start();
//        t2.start();
        MyThreadClass t3 = new MyThreadClass(0,1000000000/2,"Praca1");
        MyThreadClass t4 = new MyThreadClass(1000000000/2,1000000000,"Praca2");
        t3.start();
        t4.start();
        //Thread.yield();
        System.out.println("Operacja sekwencyjna:");
        Sequent.doAction();
        
        ExecutorService es = Executors.newFixedThreadPool(THREADS_COUNT);
        
        //FutureExample fe[] = new FutureExample[THREADS_COUNT];
        long amount = 1000000000;
        long chunk = amount/THREADS_COUNT;
        //for(int i=0;i<THREADS_COUNT-1;i++) fe[i] = new FutureExample(chunk*i, chunk*i+chunk,"Praca"+(i+1));
        //fe[THREADS_COUNT-1] = new FutureExample(chunk*(THREADS_COUNT-1),amount,"Praca"+(THREADS_COUNT-1));
        
        for(int i=0;i<THREADS_COUNT;i++) {
            FutureExample fe = new FutureExample(chunk*i, (i==THREADS_COUNT-1) ? amount : chunk*i+chunk,"Praca"+(i+1));
            Future<Object> f = es.submit(fe);
            //es.shutdown();
        }
        es.shutdown();
        if (es.isTerminated())
            System.out.println("Całkowity czas pracy (łącznie z Future): " + t.getMeasure());
    }    
}
