/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multitask;

import timeutils.MeasureTime;

/**
 *
 * @author tez
 */
public class MyThread implements Runnable{
    
    private Thread t;
    private long _start, _stop;
    private String name;

    public MyThread() {
        this(0,1000000000, String.valueOf(MeasureTime.getTimeStamp()));
    }
    
    public MyThread(long start, long stop) {
        this(start,stop, String.valueOf(MeasureTime.getTimeStamp()));
    }

    public MyThread(long start, long stop, String n) {
        this._start = start;
        this._stop = stop;
        this.name = n;
    }
    
    @Override
    public void run() {
        MeasureTime t = new MeasureTime();
        MeasureTime t1 = new MeasureTime(true);
        long l = 0;
        Long l1 = l;
        t.startMeasure();
        for (long i = _start; i < _stop; i++) 
            l+=i;
        System.out.println("Wątek " + name + ": " +t.getMeasure()+", wynik: " + l);
        t.startMeasure();
        for (Long i = _start; i < _stop; i++)
            l1+=i;
        System.out.println("Wątek " + name + ": " +t.getMeasure()+", wynik: " + l1);
        l1 = new Long(0);
        t.startMeasure();
        for (long i = _start; i < _stop; i++)
            l1+=i;
        System.out.println("Wątek " + name + ": " +t.getMeasure()+", wynik: " + l1);
        System.out.println("Wątek " + name + " kończy pracę. Całkowity czas pracy: " +t1.getMeasure());
    }
    
    public void start () {      
        if (t == null) {
           t = new Thread (this, name);
           t.start ();
        }
     }
    
}
