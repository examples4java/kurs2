/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FutureClass;

import java.util.concurrent.Callable;
import timeutils.MeasureTime;

/**
 *
 * @author tez
 */
public class FutureExample implements Callable<Object>{

    private long _start, _stop;
    private String name;

    public FutureExample() {
        this(0,1000000000,String.valueOf(MeasureTime.getTimeStamp()));
    }  
    
    public FutureExample(long _start, long _stop) {
        this(_start,_stop, String.valueOf(MeasureTime.getTimeStamp()));
    }

    public FutureExample(long _start, long _stop, String name) {
        this._start = _start;
        this._stop = _stop;
        this.name = name;
    } 
    
    
        
    @Override
    public Object call() throws Exception {
        MeasureTime t = new MeasureTime();
        MeasureTime t1 = new MeasureTime(true);
        long l = 0;
        Long l1 = l;
        t.startMeasure();
        for (long i = _start; i < _stop; i++) 
            l+=i;
        System.out.println("Wątek " + name + ": " +t.getMeasure()+", wynik: " + l);
        t.startMeasure();
        for (Long i = _start; i < _stop; i++)
            l1+=i;
        System.out.println("Wątek " + name + ": " +t.getMeasure()+", wynik: " + l1);
        l1 = new Long(0);
        t.startMeasure();
        for (long i = _start; i < _stop; i++)
            l1+=i;
        System.out.println("Wątek " + name + ": " +t.getMeasure()+", wynik: " + l1);
        System.out.println("Wątek " + name + " kończy pracę. Całkowity czas pracy: " +t1.getMeasure());
        return l1;
    }
    
    
}
