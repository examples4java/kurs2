/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timeutils;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author tez
 */
public final class MeasureTime {
    
    enum TimeOptions {
        SECONDS(0),
        MICROS(1),
        MILLIS(2),
        MINS(3),
        CURRTIME(4),
        seconds(0),
        micros(1),
        millis(2),
        mins(3),
        currtime(4);
        
        private final int val;
        
        private TimeOptions(int val) {this.val = val;}
        
        public int get() {return val;}
    }
    
    boolean running;
    long start, sum;
    Map<String, Long> timeHistory;

    public MeasureTime() {
        this(false);
    }
    
    public MeasureTime(boolean state) {
        start = sum = 0;
        //running = false;
        running = state;
        if (running) startMeasure();
    }
    
    public void startMeasure() {
        running = true;
        start = System.nanoTime();
    }
    
    public void stopMeasue() {
        running = false;
        sum = System.nanoTime();
    }
    
    public final long getMeasure() {
        return getMeasure(0);
    }
    
    public final long getMeasure(String s) {
        try {
            return getMeasure(TimeOptions.valueOf(s).get());
        }
        catch (Exception e) {
            System.err.println("Brak podanej opcji, wypisuję w domyślnym przedziale (sekundy!)");
        }
        return 0;
    }
    
    public final static long getTimeStamp() {
        return System.currentTimeMillis();
    }
    
    public final long getMeasure(int op) {
        long l = (running) ? (System.nanoTime() - start) : (sum - start);
        switch(op) {
            case 0: return TimeUnit.NANOSECONDS.toSeconds(l);
            case 1: return TimeUnit.NANOSECONDS.toMicros(l);
            case 2: return TimeUnit.NANOSECONDS.toMillis(l);
            case 3: return TimeUnit.NANOSECONDS.toMinutes(l);
            case 4: return TimeUnit.NANOSECONDS.toMillis(System.nanoTime());
        } 
        return l;
    }
     
    public void saveTime() {
        saveTime((timeHistory != null) ? String.valueOf(timeHistory.size()) : "0");
    }
    
    public void saveTime(String n) {
        if (timeHistory == null) timeHistory = new HashMap<>();
        timeHistory.put(n, getMeasure());
    }
    
    public long getSavedTime(String n) {
        return (timeHistory != null) ? timeHistory.getOrDefault(n, (long)-1) : -1;
    }
    
    public Map<String,Long> getSavedTime() {
        return (timeHistory != null) ? timeHistory : new HashMap<>();
    }
    
    public void clearSavedTime() {
        if (timeHistory == null) timeHistory = new HashMap<>();
        timeHistory.clear();
    }   
    
}
